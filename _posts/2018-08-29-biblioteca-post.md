---
layout: post
title: Biblioteca de proyectos de clase
cover: cover.jpg
date:   2018-08-29 12:00:00
categories: posts
---

[Previus](https://gitlab.com/iush/BADAT11/BADAT11)

## Biblioteca de proyectos de clase.

![Tablero](/images/board_icon.png)
[Tablero de actividades y avance](https://gitlab.com/iush/iush.gitlab.io/boards?=)

> Análisis, diseño e implementación de un sistema para registrar mediante una base de datos proyectos y recursos, busca trasversalidad para diferentes los diferentes cursos y cátedras de la IUSH, la implementacion va ha permitir a los investigadores buscar proyectos y recursos que sean a fines a su campo de investigacion.

![GitLab](/images/Gitlab_logo.png)

#### Resumen
 
Actualmente no se cuenta con un sitio web, aplicación o similar que permita revisar un históricos de proyectos realizados o en curso en donde se pueda colaborar o intervenir. Ademas, se esta desaprovechando la oportunidad de tener grupos multidisciplinarios que cooperen entre si en una misma empresa, como se da en el ambiente empresarial.
 
El sistema permite la gestión, concurso, investigación y desarrollo de proyectos para comunidad Universitaria.

Este proyecto ademas propone ser un punto de inicio, en la centralización de esfuerzos puede permitir retroalimentación con encuestas e información producto de seguimiento de proyectos.

![Desing](/images/Dialogue_project.png)

### 2. Sistema propuesto.

#### 1. Panorama 

Como una propuesta nueva se pone sobre la mesa el modelo de desarrollo de software libre en la IUSH, que permita impulsar en los estudiantes capacidades propias de la sinergia que se requiere en el desarrollo de proyectos de software. Dentro de los objetivos primordiales esta impulsar la integración con herramientas libres que tienen nuestra misma dirección y objetivos.

El uso de herramientas tecnologías, es hoy en día primordial para cumplir los objetivos en cualquier esfuerzo, estas herramientas brindan eficiencia y reducen costos, optimizando los resultados
esperados. Sin embargo el trabajo en el aula universitaria se da sin contar con herramientas que permitan implementar metodologías y técnicas de desarrollo de software.

Existen gran cantidad de proyectos tipo open source, que no solo presentan retos en su programacion, si no tambien en su diseño, administracion e implementacion, la docencia actual a nivel universitario insiste en solo plantear actividades basadas en nuevos proyectos (metodologias calcadas de los libros), dejando de lado el amplio rango de oportunidades en el mantenimiento e implementacion. Es por este mismo camino, que el estudiante suele pensar que la mayoria de los proyectos que enfrentara en su vida profecional seran implementacion desde 0, lo cual en muchas, si no en la mayoria de las ocacines es una presuncion falsa.

El trabajo colaborativo es la columna vertebral del desarrollo de software, ademas de ser una forma efectiva y eficiente de desarrollar ideas.

#### 2. Requerimientos funcionales:

- Se podrá acceder a información filtrada como ultimas noticias, anuarios de investigación y mejores propuestas.
- Las vacantes podran ser ubicadas de acuerdo a los perfiles de los investigadores.
- La información de los investigadores estará vinculada con la información registrada en una implementación de GitLab (puede ser contra la implementación principal de GitLab si se consigue convenio).
- Es posible buscar información año, por investigador, docentes investigadores.
- Los documentos estarán almacenados en repositorios enlazados por id (de GitLab).
- El sistema puedra establecer el numero de Issues 
- El documento principal de cada proyecto estará almacenado con archivo Readme.md (formato markdown), en el repositorio principal de enlace.
- La calificación darse por cátedra e individual a cada investigador.
- La calificación del proyecto es independiente a la calificación de los investigadores reclutados.
- El docente principal tendrá una opción para evaluar proyectos, podrá listar los proyectos activos y evaluarlos por condiciones como contexto, justificación, objetivos, cronograma, issues, actividades, resultados, descripción.
- Los docentes por cátedra también pueden evaluar cada proyecto.
- Una galería de proyectos, algunos proyectos tendrán un link a una pagina estática en GitLab con Jekyll
- Es posible abrir una vacante en un proyecto, buscando ayuda en por parte de algún otro investigador de otra área.
- Los proyectos deben tener tags que permitan busquedas.

#### 3. Requerimientos no funcionales.

- El demo se incluira en la cuenta gratuita de bluemix gestionada por la IUSH.
  - [projects](https://badat11startedpgrid-fantastic-baboon.mybluemix.net/projects)

![Projects Demo](/images/DemoProjects.png)

  - [orgs](https://badat11startedpgrid-fantastic-baboon.mybluemix.net/orgs)

![Projects ORG](/images/DemoOrg.png)

  - [collaboratos](https://badat11startedpgrid-fantastic-baboon.mybluemix.net/collaborators)

![Projects Collaborators](/images/DemoColl.png)

  - [perfiles](https://badat11startedpgrid-fantastic-baboon.mybluemix.net/proj_colls)

![Projects Per](/images/DemoPer.png)

### 3. Modelos del sistema

#### 1. Diseño conceptual de la base de datos.

![DiagraER](/images/project_ER.png)

#### 2. Base de datos.

[Detalle de base de datos](http://rpubs.com/oemunoz/423572)

#### 3. Diseño lógico de la base de datos.

![DiagraLO](/images/project_LO.png)

### 4. Codigo fuente y CI

Principal repositoriy:
- [Source code Repo](https://gitlab.com/iush/BADAT11/iush-project-grid)

![CI](/images/CI.png)

![Finis-bhuil](/images/FinisBuild.png)

Documentation and principal page of the project
- [Princpal page](http://iush.gitlab.io/)

### 5. Anexos.

- Riesgos:
    - Por que no ver si existe algun plugin de RedMine para bolsa de emplos y conectividad con GitLab y GitHub?

![RedMine](/images/redmine.png)

#### Bibliografia:

- https://commons.wikimedia.org/wiki/Main_Page
- https://es.wikipedia.org/wiki/Wikipedia:Portada
- http://interorganic.com.ar/josx/gitlab.pdf
- https://about.gitlab.com/images/press/git-cheat-sheet.pdf
- https://www.nersc.gov/assets/Uploads/2017-02-06-Gitlab-CI.pdf
- https://web.stevens.edu/hfslwiki/images/6/67/Redmine_Tutorial.pdf
- https://www.cs.usask.ca/~spiteri/CMPT898/notes/redmine.pdf
- https://www.bilib.es/images/stories/noticias/pdf/Analisis-Redmine.pdf
- https://reunir.unir.net/bitstream/handle/123456789/6120/IBUJES%20FACTOS%2C%20LENIN%20MAURICIO.pdf?sequence=1&isAllowed=y
- https://www.inf.utfsm.cl/~guerra/publicaciones/Gestion%20de%20Proyectos%20de%20Software.pdf
- https://repositorio.pucese.edu.ec/bitstream/123456789/33/1/MEDRANO%20COLORADO%20JOHN.pdf
