---
layout: post
title: Library Information Technology
cover: coverU.jpg
date:   2017-11-01 12:00:00
categories: posts
---

## Biblioteca de respositorios de clase.

Iniciativa que contribuye a la expansión de la investigación usando infraestructura de repositorios, que busca desarrollar liderazgo como tambien incentivar oportunidades colaborativas a través del campus IUSH.

## Comunidades en los Repositorios:

- [Arquitectura del Software](http://arsoft12.gitlab.io/)
  - [Documentos](http://arsoft12.gitlab.io/posts/)
  - [Presentaciones](http://arsoft12.gitlab.io/slide/)
- [Arquitectura del Hardware](http://arquh11.gitlab.io/)
  - [Documentos](http://arquh11.gitlab.io/posts/)
  - [Presentaciones](http://arquh11.gitlab.io/slide/)
